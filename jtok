#!/usr/bin/env python

from typing import Optional, List
import subprocess as sp
import shlex

import click
import orjson

TOKENIZERS = ["space", "chars", "whitespace", "custom"]


def run_custom_tokenizer(w: str, custom_tokenizer_cmd: str) -> List[str]:
    cmd_tokens = shlex.split(custom_tokenizer_cmd)
    pid = sp.run(args=cmd_tokens, text=True, capture_output=True, input=w)
    toks = pid.stdout.strip().split(" ")

    return toks


def tokenize(
    w: str, kind: str = "chars", custom_tokenizer_cmd: Optional[str] = None
) -> List[str]:
    if kind == "chars":
        return list(w)
    elif kind == "space":
        return w.split(" ")
    elif kind == "whitespace":
        return w.split()
    elif kind == "custom":
        assert custom_tokenizer_cmd, "Missing argument: custom_tokenizer_cmd"

        return run_custom_tokenizer(w, custom_tokenizer_cmd=custom_tokenizer_cmd)


@click.command()
@click.option("--token-type", "-t", default="chars", type=click.Choice(TOKENIZERS))
@click.option("--tokenize-all-fields", "-a", is_flag=True)
@click.option("--field-to-tokenize", "-f")
@click.option("--custom-tokenizer-command", "-c")
def main(token_type, tokenize_all_fields, field_to_tokenize, custom_tokenizer_command):
    if field_to_tokenize is None:
        assert (
            tokenize_all_fields
        ), "Won't auto-tokenize all fields! Please use -f or -a."

    with click.get_text_stream("stdin") as stdin, click.get_text_stream(
        "stdout"
    ) as stdout:
        for line in stdin:
            _json = orjson.loads(line)

            fields_to_tokenize = (
                _json.items()

                if tokenize_all_fields
                else [(field_to_tokenize, _json[field_to_tokenize])]
            )

            for text_field, string in fields_to_tokenize:
                _json[text_field] = tokenize(
                    string,
                    kind=token_type,
                    custom_tokenizer_cmd=custom_tokenizer_command,
                )

            stdout.write(f"{orjson.dumps(_json).decode('utf-8')}\n")


if __name__ == "__main__":
    main()
